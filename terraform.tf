terraform {
  required_providers {
    mongodbatlas = {
      source  = "mongodb/mongodbatlas"
      version = "1.8.2"
    }
  }
  cloud {
    organization = "zuedev"
    workspaces {
      name = "SvalBot"
    }
  }
}
