<img width="100" src="https://gitlab.com/zuedev/svalbot/-/raw/main/assets/icons/original.png" />

# SvalBot

A multi-purpose Discord bot for helping people do things. ✨

## Installation

### Public Instance

This is an invite-only bot, so if you want to use it, you'll have to fill out [this form](https://zuedev.gitlab.io/svalbot/invite-me-please-form) and wait for me to get back to you.

### Self-Hosted

Can't be bothered to wait? You can host your own instance of SvalBot by simply cloning this repository and running `npm install` and `npm start`. We also have a [Dockerfile](https://gitlab.com/zuedev/svalbot/-/blob/main/Dockerfile) if you want to run it in a container.
