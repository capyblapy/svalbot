import Color from "color";

const colours = {
  blurple: "#5865F2",
  green: "#57F287",
  yellow: "#FEE75C",
  fuchsia: "#EB459E",
  red: "#ED4245",
  white: "#FFFFFF",
  black: "#000000",
};

for (const colour in colours) {
  // change value to object with hex as only key
  colours[colour] = {
    hex: colours[colour],
  };

  let hex = colours[colour].hex;

  // add converted colour values
  colours[colour].rgbNumber = Color(hex).rgbNumber();
  colours[colour].cmyk = Color(hex).cmyk().array();
}

export default colours;
