import { insert } from "../controllers/mongodb.js";

export default async function log(message) {
  console.log(message);

  await insert("logs", {
    timestamp: new Date(),
    message,
  });
}
