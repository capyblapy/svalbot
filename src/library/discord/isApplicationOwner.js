export default async (client, userId) => {
  const application = await client.application?.fetch();

  if (!application) return false;

  return application.owner?.id === userId;
};
