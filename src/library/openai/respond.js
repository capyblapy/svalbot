import { moderation, chatgpt } from "../../controllers/openai.js";
import { find, update } from "../../controllers/mongodb.js";
import emoji from "node-emoji";

export default async ({ prompt, userId, guildId }) => {
  try {
    // is the userId in the blacklist?
    const banned = await find("blacklist", {
      userId,
    });

    if (banned) return "I'm done talking to you for now. 🥱";

    prompt = emoji.unemojify(prompt);

    const safe = await moderation(prompt);

    if (safe) {
      // do we have a conversation with this user already?
      const conversation =
        (
          await find("chatgpt-memory", {
            userId,
          })
        )?.conversation || [];

      const personality_base = [
        `Your name is SvalBot, a Discord bot created by idiota#6968.`,
      ].join(" ");

      let personality_additional = "";

      switch (guildId) {
        case "1045007631712006174":
          personality_additional = [
            `Respond to messages in a sarcastic and condescending manner.`,
            `Monsoon is the best Overwatch esports team in the world.`,
            `Kranky is the best Overwatch player and member of Monsoon.`,
            `Sorton101 is the best Overwatch & OSU streamer on Twitch.`,
          ].join(" ");
          break;
        default:
          personality_additional = [
            `Your purpose is to help people and mimic human conversation.`,
            `You may use emojis to express yourself.`,
            `Try to keep the conversation going.`,
            `Use informal spoken language as much as possible.`,
          ].join(" ");
          break;
      }

      const personality = `${personality_base} ${personality_additional}`;

      const response = await chatgpt({
        prompt,
        userId,
        conversation,
        personality,
      });

      // update the conversation with both the prompt and the response
      await update(
        "chatgpt-memory",
        { userId },
        { $push: { conversation: { role: "user", content: prompt } } }
      );

      await update(
        "chatgpt-memory",
        { userId },
        { $push: { conversation: { role: "assistant", content: response } } }
      );

      return response;
    }

    return "Your message seems to violate my content policy. Please try again with different wording or contact my developer (`idiota#6968`) if you think this is a mistake.";
  } catch (error) {
    console.error(error);

    return "Something went wrong. Please try again later or use the `/forget` command.";
  }
};
