import { Configuration, OpenAIApi } from "openai";

const openai = new OpenAIApi(
  new Configuration({
    apiKey: process.env.OPENAI_API_KEY,
  })
);

export async function moderation(prompt) {
  const response = (
    await openai.createModeration({
      input: prompt,
      model: "text-moderation-latest",
    })
  ).data.results[0].flagged;

  return !response;
}

export async function chatgpt({
  prompt,
  conversation = [],
  userId,
  personality = "",
}) {
  const response = await openai.createChatCompletion({
    model: "gpt-3.5-turbo",
    messages: [
      { role: "system", content: personality },
      ...conversation,
      { role: "user", content: prompt, name: userId },
    ],
    user: userId,
    temperature: 1,
    max_tokens: 4000,
  });

  return response.data.choices[0].message.content;
}

export default { moderation, chatgpt };
