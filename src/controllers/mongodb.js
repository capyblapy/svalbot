import { MongoClient } from "mongodb";

export async function connect() {
  const mongo = new MongoClient(process.env.MONGODB_URI, {
    retryWrites: true,
    writeConcern: "majority",
  });

  await mongo.connect();

  return mongo;
}

// insert new documents into the table
export async function insert(table, objects) {
  const mongo = await connect();

  // if the object is not an array, make it an array
  if (!Array.isArray(objects)) objects = [objects];

  const data = await mongo
    .db(process.env.ENVIRONMENT)
    .collection(table)
    .insertMany(objects);

  await mongo.close();

  return data;
}

// replace any documents that match the filter with the object
export async function set(table, filter, object) {
  const mongo = await connect();

  const data = await mongo
    .db(process.env.ENVIRONMENT)
    .collection(table)
    .updateMany(filter, { $set: object }, { upsert: true });

  await mongo.close();

  return data;
}

// update any documents that match the filter with the object
export async function update(table, filter, object) {
  const mongo = await connect();

  const data = await mongo
    .db(process.env.ENVIRONMENT)
    .collection(table)
    .updateMany(filter, object, { upsert: true });

  await mongo.close();

  return data;
}

// find all documents that match the filter
export async function find(table, filter, toArray = false) {
  const mongo = await connect();

  const data = await mongo
    .db(process.env.ENVIRONMENT)
    .collection(table)
    .find(filter)
    .toArray();

  await mongo.close();

  // if toArray is true, return the array
  if (toArray) return data;

  if (data.length === 0) return false;

  // if there is only one result, return it instead of an array
  if (data.length === 1) return data[0];

  return data;
}

// delete any documents that match the filter
export async function remove(table, filter) {
  const mongo = await connect();

  const data = await mongo
    .db(process.env.ENVIRONMENT)
    .collection(table)
    .deleteMany(filter);

  await mongo.close();

  if (data.deletedCount === 0) return false;

  return true;
}

export default { insert, set, update, find, remove };
