import {
  SlashCommandBuilder,
  EmbedBuilder,
  ChannelType,
  PermissionFlagsBits,
} from "discord.js";
import colours from "../../../config/colours.js";

export default {
  command: new SlashCommandBuilder()
    .setName("apply")
    .setDescription("Apply to join the server")
    .addStringOption((option) =>
      option
        .setName("name")
        .setDescription("Your in-game name")
        .setRequired(true)
    )
    .addIntegerOption((option) =>
      option.setName("age").setDescription("Your age").setRequired(true)
    )
    .addStringOption((option) =>
      option
        .setName("country")
        .setDescription("The country you live in currently")
        .setRequired(true)
    )
    .addStringOption((option) =>
      option
        .setName("pronouns")
        .setDescription("Your pronouns")
        .setRequired(true)
    )
    .addStringOption((option) =>
      option
        .setName("steam_url")
        .setDescription(
          "Your Steam profile URL (e.g. https://steamcommunity.com/id/username)"
        )
        .setRequired(true)
    )
    .addIntegerOption((option) =>
      option
        .setName("arma_hours")
        .setDescription("Your total hours in Arma 3")
        .setRequired(true)
    )
    .addStringOption((option) =>
      option
        .setName("ace_experience")
        .setDescription("Your experience with ACE")
        .setRequired(true)
        .addChoices(
          { name: "None", value: "None" },
          { name: "Beginner", value: "Beginner" },
          { name: "Intermediate", value: "Intermediate" },
          { name: "Advanced", value: "Advanced" }
        )
    )
    .addStringOption((option) =>
      option
        .setName("acre_or_tfar")
        .setDescription("Your experience with ACRE and/or TFAR")
        .setRequired(true)
        .addChoices(
          { name: "ACRE", value: "ACRE" },
          { name: "TFAR", value: "TFAR" },
          { name: "Both", value: "Both" },
          { name: "Neither", value: "Neither" }
        )
    ),
  async execute(interaction) {
    // do we already have an application channel for this user?
    const applicationChannelExists =
      await interaction.guild.channels.cache.find(
        (channel) =>
          channel.name ===
            `application-${interaction.user.username.toLowerCase()}` &&
          channel.type === ChannelType.GuildText
      );

    if (applicationChannelExists)
      return await interaction.reply(
        "You have already applied! Please wait for a response."
      );

    // create a new application channel under the tickets category
    const ticketsCategory = await interaction.guild.channels.cache.find(
      (channel) =>
        channel.name === "tickets" && channel.type === ChannelType.GuildCategory
    );

    const applicationChannel = await interaction.guild.channels.create({
      name: `application-${interaction.user.tag.toLowerCase()}`,
      type: ChannelType.GuildText,
      parent: ticketsCategory,
      permissionOverwrites: [
        {
          id: interaction.guild.roles.everyone.id,
          deny: [PermissionFlagsBits.ViewChannel],
        },
        {
          id: interaction.user.id,
          allow: [PermissionFlagsBits.ViewChannel],
        },
      ],
    });

    // send a message to the application channel with embed

    const applicationEmbed = new EmbedBuilder()
      .setTitle("Application")
      .setDescription(`**User ID:** \`${interaction.user.id}\``)
      .setColor(colours.blurple.rgbNumber)
      .setThumbnail(interaction.user.avatarURL());

    // convert options to fields and add to embed
    const options = interaction.options.data;
    options.forEach((option) => {
      applicationEmbed.addFields({
        name: String(option.name),
        value: String(option.value),
        inline: true,
      });
    });

    await applicationChannel.send({
      content: `Hello, ${interaction.user}! Thank you for applying to join the server. Please wait for a response from a staff member shortly. Here is a copy of your application:`,
      embeds: [applicationEmbed],
    });

    // try to notify recruiters of a new application via DM
    try {
      const recruitersRole = await interaction.guild.roles.cache.find(
        (role) => role.name === "Recruiters"
      );

      const recruiters = await interaction.guild.members.cache.filter(
        (member) => member.roles.cache.has(recruitersRole.id)
      );

      recruiters.forEach(async (recruiter) => {
        await recruiter.send({
          content: `A new application has been submitted by ${interaction.user}!`,
          embeds: [applicationEmbed],
        });
      });
    } catch (error) {
      console.log(error);
    }

    // reply to the user
    await interaction.reply({
      content: `Your application has been submitted! Please wait for a response in <#${applicationChannel.id}>.`,
      ephemeral: true,
    });
  },
};
