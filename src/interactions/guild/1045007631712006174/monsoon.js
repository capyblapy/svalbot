import { SlashCommandBuilder } from "discord.js";
import { find, insert, remove } from "../../../controllers/mongodb.js";
import monsoon from "./.monsoon.js";

export default {
  command: new SlashCommandBuilder()
    .setName("monsoon")
    .setDescription("Monsoon commands.")
    .addSubcommandGroup((subcommandGroup) =>
      subcommandGroup
        .setName("manager")
        .setDescription("Commands for Monsoon managers.")
        .addSubcommand((subcommand) =>
          subcommand
            .setName("list_absences")
            .setDescription("List upcoming absences")
        )
        .addSubcommand((subcommand) =>
          subcommand
            .setName("remove_absence")
            .setDescription("Remove an upcoming absence")
            .addStringOption((option) =>
              option
                .setName("date")
                .setDescription(
                  'Date of absence, in DD-MM-YY format (e.g. "01-03-24" for March 1, 2024)'
                )
                .setRequired(true)
            )
            .addUserOption((option) =>
              option
                .setName("user")
                .setDescription("User to remove absence for")
                .setRequired(true)
            )
        )
    )
    .addSubcommandGroup((subcommandGroup) =>
      subcommandGroup
        .setName("player")
        .setDescription("Commands for Monsoon players.")
        .addSubcommand((subcommand) =>
          subcommand
            .setName("new_absence")
            .setDescription("Report a new upcoming absence")
            .addStringOption((option) =>
              option
                .setName("date")
                .setDescription(
                  'Date of absence, in DD-MM-YY format (e.g. "01-03-24" for March 1, 2024)'
                )
                .setRequired(true)
            )
            .addStringOption((option) =>
              option
                .setName("reason")
                .setDescription("Reason for absence")
                .setRequired(true)
            )
        )
        .addSubcommand((subcommand) =>
          subcommand
            .setName("list_absences")
            .setDescription("List upcoming absences")
        )
        .addSubcommand((subcommand) =>
          subcommand
            .setName("remove_absence")
            .setDescription("Remove an upcoming absence")
            .addStringOption((option) =>
              option
                .setName("date")
                .setDescription(
                  'Date of absence, in DD-MM-YY format (e.g. "01-03-24" for March 1, 2024)'
                )
                .setRequired(true)
            )
        )
    ),
  async execute(interaction) {
    switch (interaction.options.getSubcommandGroup()) {
      case "manager": {
        // is user a manager?
        if (!monsoon.manager_discord_ids.includes(interaction.user.id))
          return await interaction.reply("You are not a Monsoon manager");

        switch (interaction.options.getSubcommand()) {
          case "list_absences": {
            const absences = await find(
              "monsoon-absences",
              {
                date: { $gte: new Date().toISOString() },
              },
              true
            );

            // if there are no absences, tell the user
            if (absences.length === 0)
              return await interaction.reply("There are no upcoming absences");

            // otherwise, send a list of absences
            await interaction.reply({
              content: absences
                .map(
                  (absence) =>
                    `${absence.user_tag} - ${
                      // convert ISO date to human-readable format
                      new Date(absence.date).toLocaleDateString("en-US", {
                        weekday: "long",
                        year: "numeric",
                        month: "long",
                        day: "numeric",
                      })
                    } - ${absence.reason}`
                )
                .join("\n"),
              ephemeral: true,
            });

            break;
          }
          case "remove_absence": {
            const date = interaction.options.getString("date");
            const user = interaction.options.getUser("user");

            // does "date" match DD-MM-YY format?
            if (!/^\d{2}-\d{2}-\d{2}$/.test(date))
              return await interaction.reply(
                "Date must be in DD-MM-YY format (e.g. 01-03-24 for March 1, 2024)"
              );

            // is date in the future?
            if (
              new Date(
                `20${date.split("-")[2]}-${date.split("-")[1]}-${
                  date.split("-")[0]
                }`
              ) < new Date()
            )
              return await interaction.reply("Date must be in the future");

            // does the user have an absence on that date?
            const absence = await find(
              "monsoon-absences",
              {
                user_id: user.id,
                date: new Date(
                  `20${date.split("-")[2]}-${date.split("-")[1]}-${
                    date.split("-")[0]
                  }`
                ).toISOString(),
              },
              true
            );

            if (absence.length === 0)
              return await interaction.reply(
                `${user.tag} does not have an absence on ${date}`
              );

            // remove the absence
            await remove("monsoon-absences", {
              user_id: user.id,
              date: new Date(
                `20${date.split("-")[2]}-${date.split("-")[1]}-${
                  date.split("-")[0]
                }`
              ).toISOString(),
            });

            await interaction.reply(`Removed ${user.tag}'s absence on ${date}`);

            break;
          }
          default: {
            await interaction.reply("Invalid subcommand");
            break;
          }
        }
        break;
      }
      case "player": {
        // is user a player or manager?
        if (
          !monsoon.player_discord_ids.includes(interaction.user.id) &&
          !monsoon.manager_discord_ids.includes(interaction.user.id)
        )
          return await interaction.reply(
            "You are not a Monsoon player or manager"
          );

        switch (interaction.options.getSubcommand()) {
          case "new_absence": {
            const date = interaction.options.getString("date");
            const reason = interaction.options.getString("reason");

            // does "date" match DD-MM-YY format?
            if (!/^\d{2}-\d{2}-\d{2}$/.test(date))
              return await interaction.reply(
                "Date must be in DD-MM-YY format (e.g. 01-03-24 for March 1, 2024)"
              );

            // is date in the future?
            if (
              new Date(
                `20${date.split("-")[2]}-${date.split("-")[1]}-${
                  date.split("-")[0]
                }`
              ).getTime() < Date.now()
            )
              return await interaction.reply("Date must be in the future");

            // insert absence into database
            await insert("monsoon-absences", {
              user_id: interaction.user.id,
              user_tag: interaction.user.tag,
              created_at: new Date().toISOString(),
              // convert DD-MM-YY to ISO format
              date: new Date(
                `20${date.split("-")[2]}-${date.split("-")[1]}-${
                  date.split("-")[0]
                }`
              ).toISOString(),
              reason,
            });

            // tell user that their absence has been recorded
            await interaction.reply(
              `Your absence for ${date} has been recorded with reason: ${reason}`
            );

            break;
          }
          case "list_absences": {
            const absences = await find(
              "monsoon-absences",
              {
                date: { $gte: new Date().toISOString() },
              },
              true
            );

            console.log(absences);

            // if there are no absences, tell the user
            if (absences.length === 0)
              return await interaction.reply("There are no upcoming absences");

            // otherwise, send a list of absences
            await interaction.reply(
              absences
                .map(
                  (absence) =>
                    `${absence.user_tag} - ${
                      // convert ISO date to human-readable format
                      new Date(absence.date).toLocaleDateString("en-US", {
                        weekday: "long",
                        year: "numeric",
                        month: "long",
                        day: "numeric",
                      })
                    }`
                )
                .join("\n")
            );

            break;
          }
          case "remove_absence": {
            const date = interaction.options.getString("date");

            // does "date" match DD-MM-YY format?
            if (!/^\d{2}-\d{2}-\d{2}$/.test(date))
              return await interaction.reply(
                "Date must be in DD-MM-YY format (e.g. 01-03-24 for March 1, 2024)"
              );

            // is date in the future?
            if (
              new Date(
                `20${date.split("-")[2]}-${date.split("-")[1]}-${
                  date.split("-")[0]
                }`
              ).getTime() < Date.now()
            )
              return await interaction.reply("Date must be in the future");

            // look for absence in database with user id and date
            const absence = await find(
              "monsoon-absences",
              {
                user_id: interaction.user.id,
                // convert DD-MM-YY to ISO format
                date: new Date(
                  `20${date.split("-")[2]}-${date.split("-")[1]}-${
                    date.split("-")[0]
                  }`
                ).toISOString(),
              },
              true
            );

            // if absence is not found, tell the user
            if (absence.length === 0)
              return await interaction.reply(
                `You do not have an absence for ${date}`
              );

            // otherwise, remove absence from database
            const deleted = await remove("monsoon-absences", {
              user_id: interaction.user.id,
              // convert DD-MM-YY to ISO format
              date: new Date(
                `20${date.split("-")[2]}-${date.split("-")[1]}-${
                  date.split("-")[0]
                }`
              ).toISOString(),
            });

            // did we delete?
            if (!deleted)
              return await interaction.reply(
                "Something went wrong while removing your absence"
              );

            // tell user that their absence has been removed
            await interaction.reply(
              `Your absence for ${date} has been removed`
            );

            break;
          }
          default: {
            await interaction.reply("Unknown subcommand");
            break;
          }
        }
        break;
      }
      default: {
        await interaction.reply("Unknown subcommand group");
        break;
      }
    }
  },
};
