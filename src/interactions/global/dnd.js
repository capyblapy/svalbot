import { SlashCommandBuilder, EmbedBuilder } from "discord.js";
import colours from "../../config/colours.js";
import { find } from "../../controllers/mongodb.js";

export default {
  command: new SlashCommandBuilder()
    .setName("dnd")
    .setDescription("D&D related commands")
    .addSubcommandGroup((subcommandGroup) =>
      subcommandGroup
        .setName("search")
        .setDescription("Search the D&D 5e database")
        .addSubcommand((subcommand) =>
          subcommand
            .setName("bestiary")
            .setDescription("Search the D&D 5e bestiary")
            .addStringOption((option) =>
              option
                .setName("query")
                .setDescription("The query to search for")
                .setRequired(true)
            )
            .addBooleanOption((option) =>
              option
                .setName("json")
                .setDescription(
                  "Whether to return the raw JSON instead of an embed"
                )
            )
        )
    ),
  async execute(interaction) {
    switch (interaction.options.getSubcommandGroup()) {
      case "search": {
        switch (interaction.options.getSubcommand()) {
          case "bestiary": {
            await searchBeastiary(interaction);
            break;
          }
          default: {
            await interaction.reply("Unknown subcommand");
            break;
          }
        }
        break;
      }
      default: {
        await interaction.reply("Unknown subcommand group");
        break;
      }
    }
  },
};

async function searchBeastiary(interaction) {
  const query = interaction.options.getString("query");
  const json = interaction.options.getBoolean("json") || false;
  const reply = await interaction.reply(`Searching for "${query}"...`);

  let results = await find("dnd-data-bestiary", {
    name: { $regex: query, $options: "i" },
  });

  if (!results) return await reply.edit(`No results found for "${query}"!`);

  // do we have more than 1 result?
  if (results.length > 1) {
    // if so, let them choose which one they want
    const embed = new EmbedBuilder()
      .setColor(colours.red.rgbNumber)
      .setTitle(`Results for "${query}"`)
      .setDescription(
        results
          .slice(0, 10)
          .map(
            (result, index) => `${index + 1}. ${result.name} (${result.source})`
          )
          .join("\n")
      );

    // do we have more than 10 results? add a note to the embed saying that there are more
    if (results.length > 10)
      embed.setFooter({
        text: `There are ${
          results.length - 10
        } more results, please refine your search!`,
      });

    await reply.edit({
      content: `Multiple results found, please choose one by typing its number (1-${
        results.length > 10 ? 10 : results.length
      })`,
      embeds: [embed],
    });

    try {
      const collected = await interaction.channel.awaitMessages({
        filter: (message) =>
          message.author.id === interaction.user.id &&
          message.content >= 1 &&
          message.content <= results.length,
        max: 1,
        time: 30000,
        errors: ["time"],
      });

      results = results[collected.first().content - 1];

      // delete the message
      await collected.first().delete();
    } catch (error) {
      await interaction.editReply({
        content:
          "You didn't choose a result in time! This message will self-destruct in 5 seconds.",
        embeds: [],
      });

      setTimeout(async () => {
        await reply.delete();
      }, 5000);

      return;
    }
  }

  if (json) {
    await reply.edit({
      content: `Result for "${query}"`,
      embeds: [],
      files: [
        {
          attachment: Buffer.from(JSON.stringify(results, null, 2)),
          name: `${results.name}.json`,
        },
      ],
    });
  } else {
    // we now only have 1 result
    const embed = new EmbedBuilder()
      .setColor(colours.red.rgbNumber)
      .setTitle(results.name)
      .setDescription(
        `${
          {
            T: "Tiny",
            S: "Small",
            M: "Medium",
            L: "Large",
            H: "Huge",
            G: "Gargantuan",
          }[results.size]
        } ${results.type?.type || results.type}, ${
          {
            L: "Lawful",
            N: "Neutral",
            C: "Chaotic",
            U: "Unaligned",
          }[results.alignment[0]] || ""
        } ${
          {
            G: "Good",
            N: "Neutral",
            E: "Evil",
          }[results.alignment[1]] || ""
        }`
      );

    // does it have a token?
    if (results.hasToken)
      embed.setThumbnail(
        encodeURI(
          `https://5e.tools/img/${results.source.trim()}/${results.name}.png`
        )
      );

    // does it have an image?
    // TODO: add a check for if the image exists, could also be png or webp
    if (results.hasFluffImages)
      embed.setImage(
        encodeURI(
          `https://5e.tools/img/bestiary/${results.source.trim()}/${
            results.name
          }.jpg`
        )
      );

    // TODO: does it have fluff text? these are stored weirdly: https://github.com/5etools-mirror-1/5etools-mirror-1.github.io/blob/1e409621515bfc291eaf981506f4cfe2e3d1f2ca/data/bestiary/fluff-bestiary-scc.json#L234-L254

    await reply.edit({
      content: `Result for "${query}"`,
      embeds: [embed],
    });
  }
}
