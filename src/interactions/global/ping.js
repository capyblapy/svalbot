import { SlashCommandBuilder } from "discord.js";

export default {
  command: new SlashCommandBuilder().setName("ping").setDescription("Pong!"),
  async execute(interaction) {
    await interaction.reply("Pong!");
  },
};
