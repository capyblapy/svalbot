import {
  SlashCommandBuilder,
  PermissionFlagsBits,
  EmbedBuilder,
} from "discord.js";
import colours from "../../config/colours.js";

export default {
  command: new SlashCommandBuilder()
    .setName("admin")
    .setDescription("Admin only commands")
    .setDefaultMemberPermissions(PermissionFlagsBits.Administrator)
    .addSubcommand((subcommand) =>
      subcommand
        .setName("debug_guild")
        .setDescription("Return debug information about the guild")
        .addStringOption((option) =>
          option
            .setName("guild_id")
            .setDescription("The guild to debug, defaults to the current guild")
        )
    ),
  async execute(interaction) {
    switch (interaction.options.getSubcommand()) {
      case "debug_guild": {
        // Fetch the guild to get the latest information
        const guildId = interaction.options.getString("guild_id");

        let guild;

        try {
          guild = guildId
            ? await interaction.client.guilds.fetch(guildId)
            : await interaction.guild.fetch();
        } catch (error) {
          console.error(error);
          return await interaction.reply("Failed to fetch guild");
        }

        let embed = new EmbedBuilder()
          .setTitle("Guild Debug")
          .setColor(colours.blurple.rgbNumber)
          .setThumbnail(guild.iconURL() ?? "https://via.placeholder.com/100")
          .addFields(
            ...[
              "id",
              "name",
              "description",
              "ownerId",
              "available",
              "memberCount",
              "premiumTier",
              "premiumSubscriptionCount",
              "preferredLocale",
              "vanityURLCode",
              "large",
            ].map((field) => ({
              name: field,
              value: String(guild[field]),
            }))
          );

        await interaction.reply({ embeds: [embed] });
        break;
      }
      default: {
        await interaction.reply("Unknown subcommand");
        break;
      }
    }
  },
};
