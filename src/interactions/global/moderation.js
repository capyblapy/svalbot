import { SlashCommandBuilder, PermissionFlagsBits } from "discord.js";

export default {
  command: new SlashCommandBuilder()
    .setName("moderation")
    .setDescription("Moderation commands")
    .setDefaultMemberPermissions(PermissionFlagsBits.Administrator)
    .addSubcommand((subcommand) =>
      subcommand
        .setName("ban_user")
        .setDescription("Ban a user after sending them a DM with the reason")
        .addUserOption((option) =>
          option
            .setName("user")
            .setDescription("The user to ban")
            .setRequired(true)
        )
        .addStringOption((option) =>
          option
            .setName("reason")
            .setDescription("The reason for the ban")
            .setRequired(true)
        )
    ),
  async execute(interaction) {
    switch (interaction.options.getSubcommand()) {
      case "ban_user": {
        const user = interaction.options.getUser("user");
        const reason = interaction.options.getString("reason");

        const messageSend = `You have been banned from ${interaction.guild.name} for the following reason: ${reason}`;

        const response = interaction.reply("Banning user...");

        try {
          await user.send(messageSend);
          await interaction.guild.members.ban(user, { reason });
          await response.edit(`Banned ${user.tag} for reason: ${reason}`);
        } catch (error) {
          console.error(error);

          await response.edit(`Something went wrong while banning ${user.tag}`);
        }

        break;
      }
      default: {
        await interaction.reply("Unknown subcommand");
        break;
      }
    }
  },
};
