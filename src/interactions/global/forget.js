import { SlashCommandBuilder } from "discord.js";
import { remove } from "../../controllers/mongodb.js";

export default {
  command: new SlashCommandBuilder()
    .setName("forget")
    .setDescription("Forget everything I know about you."),
  async execute(interaction) {
    const userId = interaction.user.id;

    await remove("chatgpt-memory", { userId });

    await interaction.reply("I have forgotten everything I know about you.");
  },
};
