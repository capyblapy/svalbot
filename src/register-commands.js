import "dotenv/config";
import { REST, Routes } from "discord.js";
import fs from "fs";

const globalCommandFiles = fs
  .readdirSync("./src/interactions/global")
  .filter((file) => file.startsWith(".") === false);
const guildCommandDirectories = fs.readdirSync("./src/interactions/guild");

const globalCommands = [];

for (const file of globalCommandFiles) {
  const data = await import(`./interactions/global/${file}`);

  globalCommands.push(data.default.command);
}

const guildCommands = [];

for (const directory of guildCommandDirectories) {
  const commandFiles = fs
    .readdirSync(`./src/interactions/guild/${directory}`)
    .filter((file) => file.startsWith(".") === false);

  guildCommands[directory] = [];

  for (const file of commandFiles) {
    const data = await import(`./interactions/guild/${directory}/${file}`);

    guildCommands[directory].push(data.default.command);
  }
}

const rest = new REST({ version: "10" }).setToken(
  process.env.DISCORD_BOT_TOKEN
);

await (async () => {
  try {
    console.log("Started refreshing application global commands.");

    await rest.put(Routes.applicationCommands(process.env.DISCORD_CLIENT_ID), {
      body: globalCommands,
    });

    console.log("✅ Successfully reloaded application global commands.");
  } catch (error) {
    console.error(error);
  }
})();

await (async () => {
  for (const directory of guildCommandDirectories) {
    try {
      console.log(
        `Started refreshing application guild (${directory}) commands.`
      );

      await rest.put(
        Routes.applicationGuildCommands(
          process.env.DISCORD_CLIENT_ID,
          directory
        ),
        {
          body: guildCommands[directory],
        }
      );

      console.log(
        `✅ Successfully reloaded application guild (${directory}) commands.`
      );
    } catch (error) {
      if (error.message === "Missing Access") {
        console.log(
          `❌ Failed to reload application guild (${directory}) commands.`
        );
      } else {
        console.error(error);
      }
    }
  }
})();

console.log("Done!");
