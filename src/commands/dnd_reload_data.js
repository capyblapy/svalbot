import isApplicationOwner from "../library/discord/isApplicationOwner.js";
import { insert, remove } from "../controllers/mongodb.js";

export default async ({ client, message, args }) => {
  // only the bot owner can use this command
  if (!(await isApplicationOwner(client, message.author.id)))
    return await message.reply("Only the bot owner can use this command.");

  const reply = await message.reply(
    "Reloading D&D data... (this may take a while)"
  );

  try {
    // first, delete all existing data
    await reply.edit("Deleting existing data...");
    await remove("dnd-data", {}); // legacy code
    await remove("dnd-data-bestiary", {});

    // then, fetch new data from 5etools
    // start with the bestiary
    await reply.edit("Fetching bestiary index...");
    const response = await fetch("https://5e.tools/data/bestiary/index.json");
    const bestiaryIndex = await response.json();

    await reply.edit("Fetching bestiary data...");

    let bestiaryCount = 0;

    for (const [key, value] of Object.entries(bestiaryIndex)) {
      const response = await fetch(`https://5e.tools/data/bestiary/${value}`);
      const bestiaryData = await response.json();

      // fixes: Invalid BulkOperation, Batch cannot be empty
      if (bestiaryData.monster.length > 0)
        await insert("dnd-data-bestiary", bestiaryData.monster);

      bestiaryCount += bestiaryData.monster.length;

      await reply.edit(
        `Fetching bestiary data... (${bestiaryCount} monsters tamed)`
      );
    }

    let finalCount = bestiaryCount;

    await reply.edit(`D&D data reloaded! 👏 (${finalCount} entries stored)`);
  } catch (error) {
    console.log(error);
    await reply.edit("An error occurred while reloading D&D data! D:");
  }
};
