import isApplicationOwner from "../library/discord/isApplicationOwner.js";
import { find, insert, remove } from "../controllers/mongodb.js";

export default async ({ client, message, args }) => {
  // only the bot owner can use this command
  if (!(await isApplicationOwner(client, message.author.id))) return;

  // is the userId in the blacklist?
  const banned = await find("blacklist", {
    userId: args[0],
  });

  if (banned) {
    // remove the userId from the blacklist
    await remove("blacklist", {
      userId: args[0],
    });

    await message.reply(`User **${args[0]}** removed from the blacklist.`);
  } else {
    // add the userId to the blacklist
    await insert("blacklist", {
      userId: args[0],
    });

    await message.reply(`User **${args[0]}** added to the blacklist.`);
  }
};
