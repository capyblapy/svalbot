import express from "express";
import cors from "cors";

export default async () => {
  // setup express
  const app = express();
  app.use(cors());

  app.get("/", (request, response) => {
    response.send("The dashboard is coming soon™!");
  });

  app.get("/api", (request, response) => {
    response.send({
      uuid: uuid(),
      message: "Hello, World!",
    });
  });

  app.listen(process.env.WEB_PORT, () => {
    console.log("Server is running on port 3000");
  });

  function uuid() {
    return "x".repeat(64).replace(/x/g, () => {
      return ((Math.random() * 16) | 0).toString(16);
    });
  }
};
