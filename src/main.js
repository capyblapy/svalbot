import "dotenv/config";
import bot from "./bot.js";
import web from "./web.js";
import log from "./library/log.js";

// log startup
log("SvalBot started");

// setup required environment variables
[
  "ENVIRONMENT",
  "DISCORD_CLIENT_ID",
  "DISCORD_BOT_TOKEN",
  "OPENAI_API_KEY",
  "MONGODB_URI",
  "WEB_PORT",
].forEach((key) => {
  if (!process.env[key])
    throw new Error(`Environment variable ${key} is not set!`);
});

await bot();
await web();
