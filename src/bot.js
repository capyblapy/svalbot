import {
  Client as DiscordClient,
  GatewayIntentBits,
  Partials,
  PermissionsBitField,
} from "discord.js";
import log from "./library/log.js";
import openaiRepond from "./library/openai/respond.js";
import { startUnleash } from "unleash-client";

export default async () => {
  // setup feature flags
  const unleash = await startUnleash({
    url: process.env.UNLEASH_API_URL,
    appName: process.env.ENVIRONMENT,
    instanceId: process.env.UNLEASH_INSTANCE_ID,
    refreshInterval: 1000 * 5,
  });

  // setup discord client
  const discord = new DiscordClient({
    intents: [...Object.values(GatewayIntentBits)],
    partials: [...Object.values(Partials)],
  });

  discord.on("ready", () => {
    // log bot's ready
    log(`Bot ready, logged in as ${discord.user.tag}!`);

    // per-second interval
    setInterval(() => {
      // leave guilds that aren't on the whitelist if it exists
      if (process.env.GUILDS_WHITELIST) {
        discord.guilds.cache.forEach(async (guild) => {
          if (!process.env.GUILDS_WHITELIST.split(",").includes(guild.id)) {
            await guild.leave();
            log(`Left guild "${guild.name}" (${guild.id})`);
          }
        });
      }
    }, 1000 * 1);
  });

  discord.on("interactionCreate", async (interaction) => {
    if (!interaction.isChatInputCommand()) return;

    // what is the command?
    const command = interaction.commandName;

    // are we in a guild? if so, check if we have a guild-specific command
    const guildId = interaction.guildId;

    let interactionFile;

    if (guildId) {
      try {
        interactionFile = await import(
          `./interactions/guild/${guildId}/${command}.js`
        );
      } catch (error) {
        // don't log ERR_MODULE_NOT_FOUND errors
        if (error.code !== "ERR_MODULE_NOT_FOUND") console.error(error);
        // no guild-specific command, use the global one
        interactionFile = await import(`./interactions/global/${command}.js`);
      }
    } else {
      // no guild, use the global command
      interactionFile = await import(`./interactions/global/${command}.js`);
    }

    try {
      // execute the command
      await interactionFile.default.execute(interaction);

      // log the command
      log(
        `Command ${command} executed by ${interaction.user.tag} in ${
          interaction.guild
            ? `guild "${interaction.guild.name}" (${interaction.guild.id})`
            : "DMs"
        }`
      );
    } catch (error) {
      // log the error
      console.error(error);

      // reply with the error
      await interaction.reply({
        content: `An error occurred! :(`,
      });
    }
  });

  discord.on("messageCreate", async (message) => {
    // ignore messages from bots
    if (message.author.bot) return;

    // don't accept DMs
    if (!message.guild)
      return await message.reply(
        "I'm sorry, but I don't accept DMs at the moment!"
      );

    // list of channels to delete messages from
    [
      "1101964286890344538", // uag guest welcome channel
    ].forEach(async (channelId) => {
      // if the message is in one of the channels and isn't from an admin
      if (
        message.channel.id === channelId &&
        !message.member.permissions.has(PermissionsBitField.Flags.Administrator)
      ) {
        return await message.delete();
      }
    });

    // if the message starts with the bot's mention
    if (
      message.mentions.has(discord.user) &&
      message.content.startsWith(`<@${discord.user.id}>`)
    ) {
      // remove the mention from the message, then trim it
      const prompt = message.content
        .replace(`<@${discord.user.id}>`, "")
        .trim();

      // does the prompt start with a command prefix?
      if (prompt.startsWith("/")) {
        // what is the command?
        const command = prompt.split(" ")[0].replace("/", "");

        // does the command exist?
        try {
          await (
            await import(`./commands/${command}.js`)
          ).default({
            client: discord,
            message,
            args: prompt.split(" ").slice(1),
          });

          return;
        } catch (error) {
          console.log(error);
        }
      }

      // check if we have feature flag checked
      if (!unleash.isEnabled("chatgpt_responses")) return;

      // not a command prompt, send it to OpenAI
      message.channel.sendTyping();

      const response = await openaiRepond({
        prompt: message.content,
        userId: message.author.id,
        guildId: message.guild.id,
      });

      await message.reply(response);

      // log the response
      await log(
        `Responded to ${message.author.tag} in ${
          message.guild
            ? `guild "${message.guild.name}" (${message.guild.id})`
            : "DMs"
        }`
      );
    }
  });

  await discord.login(process.env.DISCORD_BOT_TOKEN);
};
